package com.sevensample.seven.ui;

import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public interface FlickrUIManager {

	public void init();
	
	public void initComponents();
	
	public void show();
	
	public JButton getSearchButton();
	
	public JTextArea getResultTextArea();
	
	public JTextField getSearchTextField();
	
}
