package com.sevensample.seven.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FlickrUIManagerImpl implements FlickrUIManager {

	private JFrame jf = null;
	private JTextField searchText;
	private JButton searchButton;
	private JTextArea textQueryResult;
	
	@Override
	public void show() {
		jf.pack();
		jf.setVisible(true);
	}
	
	public void init() {
		jf = new JFrame("Flickr Activity Demo");
		searchText = new JTextField("DEXTRA");
		searchButton = new JButton("BUSCAR");
		textQueryResult = new JTextArea("");
	}
	
	@Override
	public void initComponents() {
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		searchText.setPreferredSize(new Dimension(500, 20));
		textQueryResult.setPreferredSize(new Dimension(500, 300));
		
		jf.getContentPane().add(searchText, BorderLayout.NORTH);
		jf.getContentPane().add(searchButton, BorderLayout.SOUTH);
		jf.getContentPane().add(textQueryResult, BorderLayout.CENTER);

	}

	@Override
	public JButton getSearchButton() {
		return searchButton;
	}

	@Override
	public JTextArea getResultTextArea() {
		return textQueryResult;
	}

	@Override
	public JTextField getSearchTextField() {
		return searchText;
	}

}
