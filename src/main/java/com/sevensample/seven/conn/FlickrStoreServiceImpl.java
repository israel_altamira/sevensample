package com.sevensample.seven.conn;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import org.json.JSONObject;

public class FlickrStoreServiceImpl implements FlickrStoreService {

	private static Connection connection = null;

	public Connection connect(String protocol, String host, String pUser,
			String pPassword, String pDataBase) {

		if (connection != null) {
			return connection;
		}

		String databaseURL = null;
		Properties info = new Properties();
		info.put("user", pUser);
		info.put("password", pPassword);

		try {

			if (FlickrStoreService.PROTOCOL_ORACLE.equalsIgnoreCase(protocol)) {
				
				// jdbc:oracle:thin:@localhost:1521:xe
				// jdbc:oracle:thin:@localhost:1521:mkyong
				databaseURL = protocol + host + pDataBase;
				Class.forName(FlickrStoreService.CLASS_ORACLE);
				
			} else if (FlickrStoreService.PROTOCOL_MYSQL
					.equalsIgnoreCase(protocol)) {
				
				// jdbc:mysql://localhost/DataBase
				databaseURL = protocol + host + "/" + pDataBase;
				Class.forName(FlickrStoreService.CLASS_MYSQL);
				
			}
			if (FlickrStoreService.PROTOCOL_SQLSERVER
					.equalsIgnoreCase(protocol)) {
				
				// jdbc:sqlserver://[serverName[\instanceName][:portNumber]][;property=value[;property=value]]
				// jdbc:sqlserver://localhost:1433;databaseName=AdventureWorks
				// + ";databaseName=" + pDataBase;
				info.put("databaseName", pDataBase);
				databaseURL = protocol + host;
				Class.forName(FlickrStoreService.CLASS_SQLSERVER);
				
			}

			connection = DriverManager.getConnection(databaseURL, info);
			System.out.println("Conexion con DBMS Establecida..");

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return connection;
	}

	public StoreStatus savePhotoInfo(Connection connection, JSONObject photo) {

		StoreStatus status = new StoreStatus();

		try {

			connection.setAutoCommit(false);
			
			CallableStatement callsp = connection
					.prepareCall("{ call storePhotoData(?,?,?,?) }");

			// farm{{farm-id}}.staticflickr.com/{{server-id}}/{{id}}_{{secret}}.{{format}}
			
			callsp.setInt("farm_id", photo.getInt("farm"));
			callsp.setString("server_id", photo.getString("server"));
			callsp.setString("photo_id", photo.getString("id"));
			callsp.setString("secret", photo.getString("secret"));

			callsp.registerOutParameter("farm_id", Types.INTEGER);
			callsp.registerOutParameter("server_id", Types.NVARCHAR);
			callsp.registerOutParameter("photo_id", Types.NVARCHAR);
			callsp.registerOutParameter("secret", Types.NVARCHAR);
			
			callsp.execute();
			connection.commit();
			status.setStatus(StoreStatus.CODE_SUCCESS, "Se hizo bien el commit");

		} catch (Exception e) {
			e.printStackTrace();
			status.setStatus(StoreStatus.CODE_ERROR, "Error al salvar");
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			status.setStatus(StoreStatus.CODE_ERROR, "Error al salvar");
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return status;
	}

	@Override
	public StoreStatus select(Connection connection) {
		// TODO Auto-generated method stub
		return null;
	}

}
