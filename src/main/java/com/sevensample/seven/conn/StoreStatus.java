package com.sevensample.seven.conn;

public class StoreStatus {

	public static final int CODE_SUCCESS = 0;
	public static final int CODE_ERROR = 1;

	private String msg = null;
	private int status = -1;

	public void setStatus(int status, String msg) {
		this.status = status;
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
