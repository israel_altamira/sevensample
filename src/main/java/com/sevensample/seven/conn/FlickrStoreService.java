package com.sevensample.seven.conn;

import java.sql.Connection;

import org.json.JSONObject;

public interface FlickrStoreService {

	public static String PROTOCOL_MYSQL = "jdbc:mysql://";
	public static String PROTOCOL_ORACLE = "jdbc:oracle:thin:@";
	public static String PROTOCOL_SQLSERVER = "jdbc:sqlserver://";

	public static String CLASS_MYSQL = "com.mysql.jdbc.Driver";
	public static String CLASS_ORACLE = "oracle.jdbc.driver.OracleDriver";
	public static String CLASS_SQLSERVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	// ORACLE PROPERTIES
	public static String LOCALHOST_ORACLE = "localhost:1521";
	public static String USER_ORACLE = "testuser";
	public static String PASSWORD_ORACLE = "testuser";
	public static String DDBB_ORACLE = ":xe";

	// SQL SERVER PROPERTIES
	public static String LOCALHOST_SQLSERVER = "192.168.30.150";
	public static String USER_SQLSERVER = "SA";
	public static String PASSWORD_SQLSERVER = "1q2w3e4r";
	public static String DDBB_SQLSERVER = "PRACTICAS";
	
	public Connection connect(String protocol, String pHost, String pUser,
			String pPassword, String pDataBase);
	
	public StoreStatus savePhotoInfo(Connection connection, JSONObject json);
	
	public StoreStatus select(Connection connection);
	
	/**
	 * Sample string connections
	 * 
	 * jdbc:oracle:thin:@localhost:1521:xe
	 * jdbc:oracle:thin:@localhost:1521:mkyong jdbc:mysql://localhost/DataBase
	 * jdbc:sqlserver://[serverName[\instanceName][:portNumber]][;property=value[;property=value]]
	 * jdbc:sqlserver://localhost:1433;databaseName=AdventureWorks;integratedSecurity=true;
	 * 
	 */
	
}
