package com.sevensample.seven.main;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.sevensample.seven.conn.FlickrStoreService;
import com.sevensample.seven.conn.FlickrStoreServiceImpl;

public class MainConnection {

	public static void main(String[] args) throws SQLException {

		Connection connMY = null;
		try {
			// creamos la conexion
			//
//			connMY = new FlickrStoreServiceImpl().connect(FlickrStoreService.PROTOCOL_ORACLE,
//					FlickrStoreService.LOCALHOST_ORACLE, FlickrStoreService.USER_ORACLE,
//					FlickrStoreService.PASSWORD_ORACLE, FlickrStoreService.DDBB_ORACLE);
			
			connMY = new FlickrStoreServiceImpl().connect(FlickrStoreService.PROTOCOL_SQLSERVER,
					FlickrStoreService.LOCALHOST_SQLSERVER, FlickrStoreService.USER_SQLSERVER,
					FlickrStoreService.PASSWORD_SQLSERVER, FlickrStoreService.DDBB_SQLSERVER);
			
			// establecemos que no sea autocommit,
			// asi controlamos la transaccion de manera manual
			connMY.setAutoCommit(false);
			/*
			 * instanciamos el objeto callable statement que usaremos para
			 * invocar el SP La cantidad de "?" determina la cantidad parametros
			 * que recibe el procedimiento
			 */
			CallableStatement sp = connMY
					.prepareCall("{ call NuestroProcedimientoAlmacenado(?,?,?) }");
			// cargar parametros al SP
			sp.setInt("id", 1);
			sp.setString("campo2", "Devtroce.com");
			sp.setInt("campo3", 49);
			
			sp.registerOutParameter(1, java.sql.Types.INTEGER);
			sp.registerOutParameter(2, java.sql.Types.NVARCHAR);
			sp.registerOutParameter(3, java.sql.Types.INTEGER);
			
			// ejecutar el SP
			sp.execute();
			// confirmar si se ejecuto sin errores
			connMY.commit();

			System.out.println("Success call to Store Proce:" +
					sp.getInt("id") + 
					sp.getString("campo2") + 
					sp.getInt("campo3"));
		} catch (Exception e) {
			// deshacer la ejecucion en caso de error
			connMY.rollback();
			// informar por consola
			e.printStackTrace();
		} finally {
			// cerrar la conexion
			connMY.close();
		}

	}

}
