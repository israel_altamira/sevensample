package com.sevensample.seven.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.http.HttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.json.JSONObject;

import com.savensample.seven.flickrservice.FlickrJsonService;
import com.savensample.seven.flickrservice.FlickrService;
import com.savensample.seven.flickrservice.FlickrServiceImpl;
import com.savensample.seven.flickrservice.FlickrURL;
import com.sevensample.seven.conn.FlickrStoreService;
import com.sevensample.seven.conn.FlickrStoreServiceImpl;
import com.sevensample.seven.ui.FlickrUIManager;
import com.sevensample.seven.ui.FlickrUIManagerImpl;

public class FlickrActivity {

	private FlickrService fservice = new FlickrServiceImpl();
	private FlickrJsonService fjservice = new FlickrJsonService();
	private FlickrStoreService dbservice = new FlickrStoreServiceImpl();

	private FlickrUIManager uim = new FlickrUIManagerImpl();

	public static void main(String[] args) {
		FlickrActivity f = new FlickrActivity();
		f.getUIManager().show();
		System.out.println("done");
	}

	public FlickrUIManager getUIManager() {
		return uim;
	}

	/** Called when the activity is first created. */
	public FlickrActivity() {

		uim.init();

		uim.initComponents();

		uim.getSearchButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				String search = uim.getSearchTextField().getText().toString();

				CloseableHttpClient httpclient = fservice.client();
				HttpEntity httpEntity = fservice.connect(httpclient,
						FlickrURL.getSearchPhotoURL(search));
				String searchPhotoResult = fservice.parse(httpEntity);
				uim.getResultTextArea().setText(searchPhotoResult);

				JSONObject json = fjservice.parseJson(searchPhotoResult);

				Connection connection = dbservice.connect(
						FlickrStoreService.PROTOCOL_SQLSERVER,
						FlickrStoreService.LOCALHOST_SQLSERVER,
						FlickrStoreService.USER_SQLSERVER,
						FlickrStoreService.PASSWORD_SQLSERVER,
						FlickrStoreService.DDBB_SQLSERVER);

				dbservice.savePhotoInfo(connection, json);

				// call the other services that take DB info
				// to get images from URLs
				/*
				 * threads.init(); foto = threads.getBDinfo() >>> getEntry(URL)
				 * >> thread.setAsynchHnadler( render(UIManager),
				 * _what_ToDo_When_Photo_Arrives) insiderHandler >>> List<foto>
				 * list = foto.getStream(); >>> UIManager(
				 * UIRender.setphoto(list) ) ;
				 */

				fservice.close(httpclient);

			}
		});

	}

}
