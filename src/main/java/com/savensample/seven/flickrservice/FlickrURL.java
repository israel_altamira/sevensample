package com.savensample.seven.flickrservice;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;

public final class FlickrURL {

	/*
	 * Apply your Flickr API: www.flickr.com/services/apps/create/apply/?
	 */
	public static String FLICKR_api_key = "e3172fe07fe3353146bf6bdd8ca4ac12";
	public static String FLICKR_QUERY_method = "flickr.photos.search";
	public static String FLICKR_QUERY_per_page = "1";
	public static String FLICKR_QUERY_nojsoncallback = "1";
	public static String FLICKR_QUERY_format = "json";

	public static String getSearchPhotoURL(String query) {
		URI uri = null;
		try {
			uri = new URIBuilder()
			.setScheme("http")
			.setHost("api.flickr.com")
			.setPath("/services/rest")
			.setParameter("method", FLICKR_QUERY_method)
			.setParameter("per_page", FLICKR_QUERY_per_page)
			.setParameter("nojsoncallback", FLICKR_QUERY_nojsoncallback)
			.setParameter("format", FLICKR_QUERY_format)
			.setParameter("text", query)
			.setParameter("api_key", FLICKR_api_key)
			.build();
		} catch (URISyntaxException e) {
			System.out.println("Search URL mal formada...");
			e.printStackTrace();
		}
		
		return uri.toString();
	}

	/*
	 * This are URL from FLICKR API to get a photo image:
	 * 
	 * http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
	 * http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
	 * http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)
	 * 
	 * Example http://farm8.staticflickr.com/7332/11733900423_b939a0d136.jpg
	 * 
	 * @see "Direcciones URL de origen de fotos" at
	 * http://www.flickr.com/services/api/misc.urls.html
	 */
	public static String FLICKR_PHOTO_URL = "http://farm{{farm-id}}.staticflickr.com/{{server-id}}/{{id}}_{{secret}}.{{format}}";
	public static String FLICKR_PHOTO_URL_jpg = "jpg";

	public String getDownloadPhotoUrl(Integer farmId, String serverId,
			String photoId, String secretId, String format) {
		String url = FLICKR_PHOTO_URL.replace("{{farm-id}}", farmId.toString());
		url = url.replace("{{server-id}}", serverId);
		url = url.replace("{{id}}", photoId);
		url = url.replace("{{secret}}", secretId);
		url = url.replace("{{format}}", format);
		return url;
	}

}
