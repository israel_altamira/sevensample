package com.savensample.seven.flickrservice;

import java.awt.image.BufferedImage;

import org.apache.http.HttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;

public interface FlickrService {

	public CloseableHttpClient client();
	
	/**
	 * @return HTTPEntity
	 */
	public HttpEntity connect(CloseableHttpClient httpclient, String URL);
	
	public String parse(HttpEntity httpEntity);

	public BufferedImage parseToPhoto(HttpEntity httpEntity);
	
	public void close(CloseableHttpClient httpclient);
	
}
