package com.savensample.seven.flickrservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FlickrJsonService {

	private static String PHOTOS_KEY = "photos";
	private static String PHOTO_KEY = "photo";
	
	public JSONObject parseJson(String json) {
		JSONObject JsonObject = null;
		try {

			JsonObject = new JSONObject(json);
			JSONObject Json_photos = JsonObject.getJSONObject(PHOTOS_KEY);
			JSONArray JsonArray_photo = Json_photos.getJSONArray(PHOTO_KEY);

			JSONObject photo = JsonArray_photo.getJSONObject(0);
			System.out.println("\nid: " + photo.getString("id") + "\n"
					+ "owner: " + photo.getString("owner") + "\n"
					+ "secret: " + photo.getString("secret") + "\n"
					+ "server: " + photo.getString("server") + "\n"
					+ "farm: " + photo.get("farm") + "\n" + "title: "
					+ photo.getString("title") + "\n");

			return photo;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return JsonObject;
	}
	
}
