package com.savensample.seven.flickrservice;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.imageio.ImageIO;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;



public class FlickrServiceImpl implements FlickrService {

	@Override
	public CloseableHttpClient client() {
		return HttpClients.createDefault();
	}

	@Override
	public HttpEntity connect(CloseableHttpClient httpclient, String URL) {

		HttpGet httpGet = new HttpGet(URL);
		httpGet.addHeader("accept", "application/json");
		HttpEntity httpEntity = null;

		try {

			httpEntity = httpclient.execute(httpGet, getResponseHandler());

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return httpEntity;
	}
	
	private ResponseHandler<HttpEntity> getResponseHandler() {
		
		ResponseHandler<HttpEntity> responseHandler = new ResponseHandler<HttpEntity>() {
			
			public HttpEntity handleResponse(final HttpResponse response)
					throws ClientProtocolException, IOException {
				
				int status = response.getStatusLine().getStatusCode();
				String phrase = response.getStatusLine().getReasonPhrase();
				HttpEntity entity = response.getEntity();
				
				if (entity == null) {
		            throw new ClientProtocolException("Response contains no content");
		        }
				
				if (status >= 200 && status < 300) {
					entity = new BufferedHttpEntity(entity);
				} else {
					throw new HttpResponseException(status,phrase);
				}
				return entity;
			}
		};
		
		return responseHandler;
	}

	@Override
	public String parse(HttpEntity httpEntity) {
		String qResult = null;
		try {
			if (httpEntity != null) {
				InputStream inputStream = httpEntity.getContent();
				Reader in = new InputStreamReader(inputStream);
				BufferedReader bufferedreader = new BufferedReader(in);
				StringBuilder stringBuilder = new StringBuilder();

				String stringReadLine = null;

				while ((stringReadLine = bufferedreader.readLine()) != null) {
					stringBuilder.append(stringReadLine + "\n");
				}

				qResult = stringBuilder.toString();

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return qResult;
	}

	@Override
	public BufferedImage parseToPhoto(HttpEntity httpEntity) {
		BufferedImage image = null;
		try {
			if (httpEntity != null) {
				InputStream inputStream = httpEntity.getContent();
				BufferedInputStream in = new BufferedInputStream(inputStream);
				image = ImageIO.read(in);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return image;
	}
	
	@Override
	public void close(CloseableHttpClient httpclient) {
		try {
			httpclient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
